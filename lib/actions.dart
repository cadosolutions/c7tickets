part of c7tickets;

class ActionTicket extends BackendAction {
    @GetParam
    @Liquid
    Ticket ticket;
}

class ActionTickets extends BackendAction {
    @GetParam
    @Liquid
    Project project;
}
