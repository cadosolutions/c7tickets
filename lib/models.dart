part of c7tickets;


class Project {
    Uid uid;
    One<Site> site;

    One<Workflow> workflow;
    One<Project> parent;
    String name;
}

class Workflow {
    Uid uid;
    One<Site> site;
    String name;

    List<String> states_names;
    Map<int, Map<int, String>> transition_names;
}

class Ticket {
    Uid uid;
    One<Site> site;
    One<Project> project;
    int state;

    One<Admin> assignee;
    String title;
    String description;
}

class Comment {
    One<Ticket> ticket;
    String body;
    One<File> file;
}
